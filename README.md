# <a name="up"></a>GIT FLOW:

> Przy pracy z gitem będziemy używali tzw. git flow. Czyli prostego procesu, który ma na celu dodać nową funkcjonalność do repozytorium.

Proces ten składa się z kroków:
- [Stworzenie issue](#git_issue)
- [Stworzenie brancha](#git_branch)
- [Praca na branchu (dodanie commitów)](#git_workBranch)
- [Otworzenie merge requesta](#git_pullRequest)
- [Dyskusja oraz code review](#git_codeReview)
- [Merge](#git_merge)
- [Deploy](#git_deploy)


## <a name="git_issue"></a>Stworzenie issue
[Idź do góry](#up)

Stworzenie issue by zidentyfikować problem lub nową funkcjonalność i początkowo przedyskutować go właśnie w issue (R&D). Po tym wszystkim jeżeli zajdzie potrzeba trzeba zacząć wdrażać zmiany (czyt. stworzyć brancha i merge requesta).

## <a name="git_branch"></a>Stworzenie brancha
[Idź do góry](#up)

Branching pomaga przy pracy na jednym repozytorium w kilka osób. Więc, gdy nadejdzie taka chwila trzeba stworzyć nowego brancha na którym będziemy pracować.

- Branch ten musi być stworzony z najnowszej wersji brancha `master` i zawsze tworzymy z brancha `master`
- Pamiętaj o dobrym opisie brancha by było jasne nad czym pracujesz dla innych członków zespołu

Aby utworzyć branch należy być w repozytorium `master` i od niego wyjść `git checkout -b #numerIssue-nazwaBrancha`. #numerIssue. - powiązanie ze stworzonym issue w repo.

Aby przełączyć się pomiędzy branchami `git checkout nazwaBrancha`.

Aby sprawdzić ile mamy aktywnych branchy i na jakim pracujmey to `git branch`

Aby sprawdzić wszystkie branche na repo (remote) `git branch -a`

## <a name="git_workBranch"></a>Praca na branchu
[Idź do góry](#up)

Po stworzeniu brancha czas na pracę nad funkcjonalnością. Commits(rewizje) - są historią pracy nad funkcjonalnością więc pamiętaj, aby ich opisy były zrozumiałe dla osób które je przeglądały.

Jeżeli zrobimy już jakieś zmiany musimy dodać zmiany do repo, więc `git add .` -> `git commit -m "Nazwa commita #No."` -> `git push git push --set-upstream origin #numerIssue-nazwaBrancha`

## <a name="git_mergeRequest"></a>Otworzenie merge requesta
[Idź do góry](#up)

Merge request pozwala stworzyć miejsce, gdzie jest historia wprowadzanych zmian widoczna dla członków zespołu oraz możesz wraz z innymi dyskutować nad wprowadzaną funkcjonalnością. Pomoc/code-review wszystko dzieje się w merge-requeście więc najlepiej jak powstanie on jak najszybciej po stworzeniu nowego feature brancha.

Po wejściu w merge request należy wyznaczyć osobę która, będze code Review i Assignent. W nazwie MR należy napisać na samym początku `WIP:` jeśli dalej pracujumy nad funkcjonalnością. Jeśli feature jest gotowy to wtedy zdejmujemy `WIP` i przechodzimy do CR.

W edycji merge request należy powiązać z issue `#numerIssue` -> pozwoli nam to od razu zamknąć issue po zamknięciu brancha.

## <a name="git_codeReview"></a>Dyskusja oraz code review
[Idź do góry](#up)

W tej fazie mogą być pytania od osób sprawdzających kod. Może brakujące testy lub jakieś klasy. Osoby sprawdzające kod mogą poprosić o zmiany lub same je wykonać w ramach pomocy lub dodać komentarze przy kodzie wraz ze swoimi propozycjami.

Osoba sprawdzająca pulluje sobie repo i przełącza się na branch i sprawdza działanie funkcjonalności i zostawia komentarze jeżeli coś się nie zgadza z założeniami.

## <a name="git_merge"></a>Merge
[Idź do góry](#up)

Jeżeli wszystko do tej pory jest w porządku trzeba połączyć nasz feature branch z master'em. 

- Przy merge robimy 'squash' commitów (łączymy w jeden wszystkie commity) i rebase'ujemy go na master branch

## <a name="git_deploy"></a>Deploy
[Idź do góry](#up)

Faza sprawdzenia brancha na środowisku zbliżonym do środowiska produkcyjnego w celu sprawdzenia czy funkcjonalność lub cała aplikacja działa poprawnie.
